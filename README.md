# State of application

[![Netlify Status](https://api.netlify.com/api/v1/badges/c4ffedd7-527e-40e8-9259-99ae5bb8914d/deploy-status)](https://app.netlify.com/sites/magic-tools/deploys)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Composition of App

### Components

Design are imported from my personnal library [Npm](https://www.npmjs.com/package/@horsty/library) or [Git](https://gitlab.com/Horsty/horsty-components) you can use it by run

```
npm i @horsty/library
```

If you are on `localhost` you need the library on same level directory and do

```
cd MagicTools
npm i --save ../library
```
