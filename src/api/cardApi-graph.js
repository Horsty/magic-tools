import { handleResponse, handleError } from "./apiUtils";
const baseUrl = process.env.API_URL + "/cards/";

export function getCards() {
  return fetch(baseUrl).then(handleResponse).catch(handleError);
}
export function searchCards(name) {
  return fetch("http://localhost:8080/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: `{searchCard(name:"${name}") {id,name,mcmId,multiverseId,printingSet,foreignName {name},set {name}}}`,
    }),
  })
    .then(handleResponse)
    .catch(handleError);
}
export function getCard(id) {
  return fetch("http://localhost:8080/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: `{card(id:"${id}") {id,multiverseId,mcmId,name,printingSet,foreignName {name},set {name}}}`,
    }),
  })
    .then(handleResponse)
    .catch(handleError);
}
export function saveCard(card) {
  return fetch(baseUrl + (card.id || ""), {
    method: card.id ? "PUT" : "POST", // POST for create, PUT to update when id already exists.
    headers: { "content-type": "application/json" },
    body: JSON.stringify(card),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function deleteCard(cardId) {
  return fetch(baseUrl + cardId, { method: "DELETE" })
    .then(handleResponse)
    .catch(handleError);
}
