import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Spinner from "../common/Spinner";
import * as cardActions from "../../redux/actions/cardActions";
import styles from "./List.module.scss";
import ViewCard from "../common/ViewCard";

const CardListsPage = ({
  match,
  searchCards,
  loading,
  getCard,
  resetCards,
  resetCard,
  cards,
}) => {
  const { searchValue } = match.params;
  useEffect(() => {
    return () => {
      resetCards();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (searchValue && searchValue.length && cards.length === 0) {
      searchCards(searchValue);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchValue]);

  function handleClickCard(event) {
    const cardId = event.currentTarget.dataset.key;
    resetCard();
    getCard(cardId);
  }

  return (
    <section>
      <h2>
        Résultats de la recherche
          </h2>
      {cards && cards.length !== 0 ? (
        <div className={styles.resultat}>
          {cards.map((card) => (
            <div className={styles.card}
              key={card.id}>
              <ViewCard
                id={card.id}
                handleClickCard={handleClickCard}
                title={card.printed_name || card.name}
                img={
                  (card.image_uris && card.image_uris.border_crop) ||
                  card.card_faces[0].image_uris.border_crop
                }
                to={"/cards/" + card.name.toLowerCase() + "/" + card.id}
              />
            </div>
          ))}
        </div>
      ) : (
        <div>{searchValue && loading ? <Spinner /> : null}</div>
      )}
    </section>
  );
};

CardListsPage.propTypes = {
  match: PropTypes.object.isRequired,
  cards: PropTypes.array,
  loading: PropTypes.bool,
  searchCards: PropTypes.func,
  getCard: PropTypes.func,
  resetCards: PropTypes.func,
  resetCard: PropTypes.func,
  updateCardsResult: PropTypes.func,
  resetAutocomplete: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    cards: (state.cards && state.cards.resultList) || [],
    loading: state.apiCallsInProgress > 0,
  };
}
const mapDispatchToProps = {
  searchCards: cardActions.searchCards,
  getCard: cardActions.getCard,
  resetCards: cardActions.resetCards,
  resetCard: cardActions.resetCard,
  updateCardsResult: cardActions.updateCardsResult,
  resetAutocomplete: cardActions.resetAutocomplete,
};

export default connect(mapStateToProps, mapDispatchToProps)(CardListsPage);
