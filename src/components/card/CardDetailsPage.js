import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import * as cardActions from "../../redux/actions/cardActions";
import styles from "./CardDetailsPage.module.scss";
import CardSkeleton from "../common/CardSkeleton";


const CardDetailsPage = ({
  match,
  getCard,
  resetCard,
  loading,
  card,
}) => {
  const { id } = match.params;

  const [imageStatus, setImageStatus] = useState("loading");
  const handleImageErrored = () => setImageStatus("error");
  const handleImageLoaded = () => setImageStatus("success");

  useEffect(() => {
    if (!card && id && !loading) {
      getCard(id);
    }
    return () => {
      resetCard();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const imageStyle = imageStatus !== "success" ? { display: "none" } : {};
  return (
    <main>
      <div>
        <div container="true">
          <div
            item="true"
            xs={12}
            sm={12}
            md={4}
            lg={4}
            xl={4}
            className={styles.box}
          >
            {imageStatus !== "success" && (
              <div className={styles.skeleton}>
                <CardSkeleton />
              </div>
            )}
            {(card && card.id) && (
              <img
                onLoad={handleImageLoaded}
                onError={handleImageErrored}
                className={styles.boxImage}
                style={imageStyle}
                alt="Scryfall card ressource"
                src={`https://api.scryfall.com/cards/${card.id
                  }?format=image`}
              />
            )}
          </div>
        </div>
      </div>
    </main >
  );
};

CardDetailsPage.propTypes = {
  match: PropTypes.object.isRequired,
  card: PropTypes.object,
  overviewCard: PropTypes.object,
  loading: PropTypes.bool,
  getCard: PropTypes.func.isRequired,
  resetCard: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    card: state.card && state.card.completeDetails,
    overviewCard: state.card && state.card.overviewDetails,
    loading: state.apiCallsInProgress > 0,
  };
}
const mapDispatchToProps = {
  getCard: cardActions.getCard,
  resetCard: cardActions.resetCard,
};

export default connect(mapStateToProps, mapDispatchToProps)(CardDetailsPage);
