import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as cardActions from "../../redux/actions/cardActions";
import { Link, withRouter } from "react-router-dom";
import Autocomplete from "./Autocomplete";
import styles from "./Autocomplete.module.scss";

const AutocompleteCardsSearch = (
  {
    getCard,
    searchCards,
    cards,
    updateCardsResult,
    history,
    resetAutocomplete,
    updateCardResult,
    submitForm,
  },
  ...props
) => {
  // State and setter for search term
  const [card, setCard] = useState({ ...props.card });
  const [cursorOnCard, setCursorOnCard] = useState({});
  const [cursor, setCursor] = useState(-1);
  const [cursorOnMore, setCursorOnMore] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    setCursor(-1); // reset cursor
  }, [cards]);

  useEffect(() => {
    if (cards[cursor]) {
      setCursorOnCard(cards[cursor]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cursor]);

  useEffect(() => {
    if (submitForm > 0 && card.name) {
      history.replace(`/cards/${card.name}`);
      setCard({ name: "" });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submitForm]);

  const onKeyDown = (event) => {
    const maxHeight = cards.length < 10 ? cards.length - 1 : 9;
    if ("ArrowUp" === event.key) {
      // je remonte au debut
      if (cursor - 1 > -1) {
        setCursor(cursor - 1);
      }
      if (cursorOnMore) {
        setCursorOnMore(false);
      }
    }
    if ("ArrowDown" === event.key) {
      // je descend en bas de liste
      if (cursor + 1 <= maxHeight) {
        setCursor(cursor + 1);
      }
      if (maxHeight === 9 && !cursorOnMore && cursor + 1 > maxHeight) {
        setCursorOnMore(true);
        setCursor(cursor + 1);
      }
    }
    if ("Enter" === event.key) {
      if (cursorOnMore) {
        event.preventDefault();
        history.replace("/cards/" + card.name.toLowerCase());
      } else if (cursorOnCard && cursor !== -1) {
        event.preventDefault();
        history.replace(
          "/cards/" + cursorOnCard.name.toLowerCase() + "/" + cursorOnCard.id
        );
      }
    }
  };
  /**
   * Event when user type his research
   * @param {*} event
   */
  function handleChange(event) {
    const { value } = event.target;
    setCard({ name: value });
    if (value && value.length >= 3) {
      setSearchTerm(value);
    } else if (cards.length > 0) {
      resetAutocomplete();
      cards = [];
    }
  }

  /**
   * Event when user click on specific result
   * @param {*} event
   */
  function handleClickCard(event) {
    const cardId = event.target.dataset.key;
    setCard({ name: "" });
    const card = cards.find((card) => card.id === cardId);
    updateCardResult(card);
    resetAutocomplete();
    getCard(cardId);
  }

  /**
   * Event when user submit the form
   * @param {*} event
   */
  function onSearch(event) {
    event.preventDefault();
    if (searchTerm.length === 0) return;
    setSearchTerm("");
    updateCardsResult();
    if (cards.length > 0) {
      resetAutocomplete();
      cards = [];
    }
    history.replace(`/cards/${card.name}`);
    setCard({ name: "" });
  }

  /**
   * Use when user click outside the autocomplete list result
   */
  function handleOutsideClick() {
    resetAutocomplete();
  }

  /**
   * Looks of result of autocomplete research
   * @param {*} card
   */
  function displayItems(card) {
    const isActive = cursorOnCard.id === card.id;
    return (
      <span
        key={card.id}
        className={`app-padding-sm ${isActive ? styles.active : ""}`}
      >
        <Link
          className={isActive ? styles.activeLink : ""}
          data-key={card.id}
          to={"/cards/" + card.name.toLowerCase() + "/" + card.id}
          onClick={handleClickCard}
        >
          {(card && card.printed_name) || card.name}
        </Link>
      </span>
    );
  }

  if (searchTerm.length < 3 && cards && cards.length > 0) {
    /* Permet de ne pas afficher de liste lors qu'il y à des cards dans le store */
    cards = [];
  }

  return (
    <Autocomplete
      onChange={handleChange}
      onClick={handleClickCard}
      onSubmit={onSearch}
      searchValue={card.name || ""}
      searchTerm={searchTerm}
      onDebounce={searchCards}
      results={cards}
      displayItems={displayItems}
      outsideClick={handleOutsideClick}
      onKeyDown={onKeyDown}
      cursorOnMore={cursorOnMore}
    />
  );
};

AutocompleteCardsSearch.propTypes = {
  search: PropTypes.bool,
  card: PropTypes.object,
  searchCards: PropTypes.func.isRequired,
  getCard: PropTypes.func.isRequired,
  resetAutocomplete: PropTypes.func.isRequired,
  updateCardsResult: PropTypes.func.isRequired,
  resetCards: PropTypes.func.isRequired,
  cards: PropTypes.array,
  results: PropTypes.array,
  autocompleteSearchDebounced: PropTypes.number,
  loading: PropTypes.bool,
  history: PropTypes.object,
};

function mapStateToProps(state) {
  const searching = false;
  const card = { name: "" };
  return {
    card,
    searching,
    cards: (state.cards && state.cards.autocompleteList) || [],
    results: (state.cards && state.cards.resultList) || [],
    loading: state.apiCallsInProgress > 0,
  };
}

const mapDispatchToProps = {
  searchCards: cardActions.autocompleteCards,
  getCard: cardActions.getCard,
  updateCardsResult: cardActions.updateCardsResult,
  resetAutocomplete: cardActions.resetAutocomplete,
  resetCards: cardActions.resetCards,
  updateCardResult: cardActions.updateCardResult,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AutocompleteCardsSearch));
