import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "../common/Spinner";
import useDebounce from "./use-debounce";
import styles from "./Autocomplete.module.scss";
import { HoliTextInput } from "@horsty/library";

const Autocomplete = ({
  onChange,
  onSubmit,
  searchTerm,
  onDebounce,
  searchValue,
  loading,
  results,
  displayItems,
  outsideClick,
  onKeyDown,
  cursorOnMore,
}) => {
  const debouncedSearchTerm = useDebounce(searchTerm, 500);
  const node = useRef();
  useEffect(() => {
    if (debouncedSearchTerm) {
      onDebounce(debouncedSearchTerm);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedSearchTerm]);

  const handleClick = (e) => {
    if (node.current.contains(e.target)) {
      // inside click
      return;
    }
    // outside click
    const { autocomplete } = node.current.dataset;
    if (autocomplete === "true") {
      outsideClick();
    }
  };
  useEffect(() => {
    // add when mounted
    document.addEventListener("mousedown", handleClick);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div
      className={styles.autocomplete}
      ref={node}
      data-autocomplete={
        (!loading && results && results.length !== 0) ||
        (searchValue && loading)
      }
    >
      <form onSubmit={onSubmit} className={styles.form}>
        <HoliTextInput
          name="name"
          placeholder="Votre recherche..."
          value={searchValue || ""}
          onChange={onChange}
          onKeyDown={onKeyDown}
        />
      </form>
      {!loading && results && results.length !== 0 ? (
        <div className={styles.autocomplete__results}>
          {results.slice(0, 10).map(displayItems)}
          {results.length > 10 ? (
            <span
              className={`app-padding-sm ${styles.seeAll} ${cursorOnMore ? styles.active : ""
                }`}
              onClick={onSubmit}
            >
              Voir plus...
            </span>
          ) : null}
        </div>
      ) : null}
      {searchValue && loading ? (
        <div className={styles.autocomplete__spinner}>
          <Spinner />
        </div>
      ) : null}
    </div>
  );
};

Autocomplete.propTypes = {
  loading: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  searchTerm: PropTypes.string.isRequired,
  onDebounce: PropTypes.func.isRequired,
  searchValue: PropTypes.string.isRequired,
  results: PropTypes.array.isRequired,
  displayItems: PropTypes.func.isRequired,
  outsideClick: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    loading: state.apiCallsInProgress > 0,
  };
}

export default connect(mapStateToProps)(Autocomplete);
