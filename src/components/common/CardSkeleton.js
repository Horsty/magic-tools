import React from "react";
import styles from "./CardSkeleton.module.scss";

const CardSkeleton = () => {
  return <div className={styles.card}></div>;
};

export default CardSkeleton;
