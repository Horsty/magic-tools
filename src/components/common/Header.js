import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./Header.module.scss";
import { HoliSvgLogo } from "@horsty/library";

const Header = () => {
  const activeStyle = { color: "#F15B2A" };
  return (
    <nav className={styles.navHeader}>
      <NavLink className="app-margin-right-lg" exact to="/">
        <HoliSvgLogo />
      </NavLink>
      <NavLink className={`${styles.navlink} app-margin-right-base`} exact to="/" activeStyle={activeStyle}>
        Accueil
      </NavLink>
      {" | "}
      <NavLink className={`${styles.navlink} app-margin-right-base app-margin-left-base`} to="/cards" activeStyle={activeStyle}>
        Recherche
      </NavLink>
      {" | "}
      <NavLink className={`${styles.navlink} app-margin-left-base`} exact to="/about" activeStyle={activeStyle}>
        À propos
      </NavLink>
    </nav>
  );
};

export default Header;
