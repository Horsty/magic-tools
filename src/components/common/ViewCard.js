import React from "react";
import PropTypes from "prop-types";
import styles from "./ViewCard.module.scss";
import { HoliCard } from "@horsty/library";
import { Link } from "react-router-dom";

const ViewCard = ({ img, title, id, handleClickCard, to }) => {
  const cardContent = () => {
    return (
      <div
        className={`${styles.media}`}>
        <img
          className={`${styles.imgMedia}`}
          src={img}
          title={title}
          alt={title}
        />
      </div>

    )
  };

  return (
    <div className={styles.card}>
      <Link
        className={styles.link}
        data-key={id}
        onClick={handleClickCard}
        to={to}
        underline="none"
      >
        <HoliCard
          title={title}
          content={cardContent()}
        />
      </Link>
    </div>
  );
};

ViewCard.propTypes = {
  title: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  content: PropTypes.string,
  id: PropTypes.string.isRequired,
  handleClickCard: PropTypes.func.isRequired,
};

export default ViewCard;
