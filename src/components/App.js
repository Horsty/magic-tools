import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import AboutPage from "./about/AboutPage";
import Header from "./common/Header";
import PageNotFound from "./PageNotFound";
import AutocompleteCardsSearch from "./autocomplete/AutocompleteCardsSearch";
import CardDetailsPage from "./card/CardDetailsPage";
import CardListsPage from "./result/List";
import HomePage from "./home/HomePage";
import { PropTypes } from "prop-types";
import styles from "./App.module.scss";
import { HoliToast } from "@horsty/library";
import { connect } from "react-redux";

const exclusionArray = ["/"];

const App = ({ location, noResult }) => {
  const isHomePage = !(exclusionArray.indexOf(location.pathname) < 0);
  return (
    <div className={`${styles.container}`}>
      {noResult && <div className={styles.toast}><HoliToast
        text="Aucun résultat"
        type="alert"
        hideClose={false}
      /></div>}
      <div className={styles.header}>
        <Header />
        {!isHomePage && <AutocompleteCardsSearch />}
      </div>
      <div className={styles.wrapper}>
        <div
          className={`${styles.content} ${isHomePage ? styles.content_inline : null
            }`}
        >
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/about" component={AboutPage} />
            <Route path="/cards/:name/:id" component={CardDetailsPage} />
            <Route path="/cards/:searchValue" component={CardListsPage} />
            <Route path="/cards" component={CardListsPage} />
            <Route component={PageNotFound} />
          </Switch>
        </div>
        <div className={styles.footer}>
          <div className={styles.logo}>Logo</div>
          <div className={styles.elem1}>Lorem ipsum</div>
          <div className={styles.company}>Company</div>
          <div className={styles.info}>Futher information</div>
          <div className={styles.socialMedia}>Social media</div>
        </div>
      </div>
    </div>
  );
}
App.propTypes = {
  location: PropTypes.object.isRequired,
  noResult: PropTypes.bool,
};
function mapStateToProps(state) {
  return {
    noResult: state.cards && state.cards.noResult,
  };
}

export default connect(mapStateToProps)(withRouter(App));
