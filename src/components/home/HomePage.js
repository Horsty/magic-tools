import React, { useState } from "react";
import AutocompleteCardsSearch from "../autocomplete/AutocompleteCardsSearch";
import { HoliCard, HoliButton } from "@horsty/library";

const HomePage = () => {
  const [search, setSearch] = useState(0);
  const onClick = () => {
    setSearch(search + 1);
  };

  const button = (
    <HoliButton text="Rechercher" type="primary" onClick={onClick} />
  );
  const input = <AutocompleteCardsSearch submitForm={search} />;
  return (
    <div>
      <HoliCard title="Rechercher une carte" content={input} footer={button} />
    </div>
  );
};

export default HomePage;
