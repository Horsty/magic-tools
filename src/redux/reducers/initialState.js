const initialState = {
  cards: {
    autocompleteList: [],
    resultList: [],
    noResult: false,
  },
  apiCallsInProgress: 0,
  card: {
    overviewDetails: null,
    completeDetails: null,
  },
};
export default initialState;