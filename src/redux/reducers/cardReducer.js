import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function cardReducer(state = initialState.card, action) {
  switch (action.type) {
    case types.UPDATE_CARD_RESULT:
      return {
        ...state,
        overviewDetails: action.card,
      };
    case types.GET_CARD_SUCCESS:
      return { ...state, completeDetails: action.card };
    case types.RESET_CARD:
      return {
        ...state,
        overviewDetails: initialState.card,
        completeDetails: initialState.card,
      };
    default:
      return state;
  }
}
