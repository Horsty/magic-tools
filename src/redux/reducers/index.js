import { combineReducers } from "redux";
import cards from "./cardsReducer";
import apiCallsInProgress from "./apiStatusReducer";
import card from "./cardReducer";

const rootReducer = combineReducers({
  cards,
  card,
  apiCallsInProgress,
});

export default rootReducer;
