import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function cardsReducer(state = initialState.cards, action) {
  switch (action.type) {
    case types.SEARCH_CARDS_SUCCESS:
      return { ...state, resultList: action.cards };
    case types.SEARCH_AUTOCOMPLETE_CARDS_SUCCESS:
      return { ...state, autocompleteList: action.cards };
    case types.RESET_CARDS_AUTOCOMPLETE_LIST:
      return { ...state, autocompleteList: [] };
    case types.RESET_CARDS_LIST:
      return { ...state, resultList: [] };
    case types.UPDATE_CARDS_RESULT_LIST:
      return {
        ...state,
        resultList: state.autocompleteList,
      };
    case types.NO_RESULT:
      return {
        ...state,
        noResult: true
      }
      case types.ON_RESULT:
        return {
          ...state,
          noResult: false
        }
    default:
      return state;
  }
}
