import cardsReducer from "./cardsReducer";
import * as actions from "../actions/cardActions";

it("should add card when passed CREATE_CARD_SUCCESS", () => {
  // arrange
  const initialState = [
    {
      name: "A"
    },
    {
      name: "B"
    }
  ];

  const newCard = {
    name: "C"
  };

  const action = actions.createCardSuccess(newCard);

  // act
  const newState = cardsReducer(initialState, action);

  // assert
  expect(newState.length).toEqual(3);
  expect(newState[0].name).toEqual("A");
  expect(newState[1].name).toEqual("B");
  expect(newState[2].name).toEqual("C");
});

it("should update card when passed UPDATE_CARD_SUCCESS", () => {
  // arrange
  const initialState = [
    { id: 1, name: "A" },
    { id: 2, name: "B" },
    { id: 3, name: "C" }
  ];

  const card = { id: 2, name: "New name" };
  const action = actions.updateCardSuccess(card);

  // act
  const newState = cardsReducer(initialState, action);
  const updatedCard = newState.find(a => a.id == card.id);
  const untouchedCard = newState.find(a => a.id == 1);

  // assert
  expect(updatedCard.name).toEqual("New name");
  expect(untouchedCard.name).toEqual("A");
  expect(newState.length).toEqual(3);
});
