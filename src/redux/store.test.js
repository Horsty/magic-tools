import { createStore } from "redux";
import rootReducer from "./reducers";
import initialState from "./reducers/initialState";
import * as cardActions from "./actions/cardActions";

it("Should handle creating cards", function () {
  // arrange
  const store = createStore(rootReducer, initialState);
  const card = {
    name: "Clean Code",
  };

  // act
  const action = cardActions.createCardSuccess(card);
  store.dispatch(action);

  // assert
  const createdCard = store.getState().cards[0];
  expect(createdCard).toEqual(card);
});
