import * as cardActions from "./cardActions";
import * as types from "./actionTypes";
import { cards } from "../../../tools/mockData";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";

// Test an async action
const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe("Async Actions", () => {
  afterEach(() => {
    fetchMock.restore();
  });

  // describe("Load Cards Thunk", () => {
  //   it("should create BEGIN_API_CALL and LOAD_CARDS_SUCCESS when loading cards", () => {
  //     fetchMock.mock("*", {
  //       body: cards,
  //       headers: { "content-type": "application/json" }
  //     });

  //     const expectedActions = [
  //       { type: types.BEGIN_API_CALL },
  //       { type: types.LOAD_CARDS_SUCCESS, cards }
  //     ];

  //     const store = mockStore({ cards: [] });
  //     return store.dispatch(cardActions.loadCards()).then(() => {
  //       expect(store.getActions()).toEqual(expectedActions);
  //     });
  //   });
  // });
});

describe("createCardSuccess", () => {
  it("should create a CREATE_CARD_SUCCESS action", () => {
    //arrange
    const card = cards[0];
    const expectedAction = {
      type: types.CREATE_CARD_SUCCESS,
      card,
    };

    //act
    const action = cardActions.createCardSuccess(card);

    //assert
    expect(action).toEqual(expectedAction);
  });
});
