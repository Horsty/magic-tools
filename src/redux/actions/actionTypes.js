export const CREATE_CARD = "CREATE_CARD";
export const LOAD_CARDS_SUCCESS = "LOAD_CARDS_SUCCESS";
export const LOAD_AUTHORS_SUCCESS = "LOAD_AUTHORS_SUCCESS";
export const CREATE_CARD_SUCCESS = "CREATE_CARD_SUCCESS";
export const UPDATE_CARD_SUCCESS = "UPDATE_CARD_SUCCESS";
export const BEGIN_API_CALL = "BEGIN_API_CALL";
export const API_CALL_ERROR = "API_CALL_ERROR";
export const GET_CARD_SUCCESS = "GET_CARD_SUCCESS";
export const SEARCH_CARDS_SUCCESS = "SEARCH_CARDS_SUCCESS";
export const SEARCH_AUTOCOMPLETE_CARDS_SUCCESS =
  "SEARCH_AUTOCOMPLETE_CARDS_SUCCESS";
export const UPDATE_CARDS_RESULT_LIST = "UPDATE_CARDS_RESULT_LIST";
export const UPDATE_CARD_RESULT = "UPDATE_CARD_RESULT";
export const RESET_CARDS_AUTOCOMPLETE_LIST = "RESET_CARDS_AUTOCOMPLETE_LIST";
export const RESET_CARDS_LIST = "RESET_CARDS_LIST";
export const RESET_CARD = "RESET_CARD";
export const NO_RESULT = "NO_RESULT";
export const ON_RESULT = "ON_RESULT";

// By convention, actions that end in "_SUCCESS" are assumed to have been the result of a completed
// Api call. But since we're doing an optimistic delete, we're hiding loading state.
// So this action name deliberately omits the "_SUCCESS" suffix.
//  If it had one, our apiCallsInProgress counter would be decremented below zero
// because we're not incrementing the number of apiCallInProgress when the delete request begins.
export const DELETE_CARD_OPTIMISTIC = "DELETE_CARD_OPTIMISTIC";
