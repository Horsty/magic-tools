import * as types from "./actionTypes";
import * as cardApi from "../../api/cardApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function fillCardsResultList() {
  return { type: types.UPDATE_CARDS_RESULT_LIST };
}
export function fillCardResult(card) {
  return { type: types.UPDATE_CARD_RESULT, card };
}
export function resetCardsList() {
  return { type: types.RESET_CARDS_LIST };
}
export function resetCardResult() {
  return { type: types.RESET_CARD };
}
export function resetCardsAutocompleteList() {
  return { type: types.RESET_CARDS_AUTOCOMPLETE_LIST };
}
export function loadCardsSuccess(cards) {
  return { type: types.LOAD_CARDS_SUCCESS, cards };
}
export function getCardSuccess(card) {
  return { type: types.GET_CARD_SUCCESS, card };
}
export function searchCardsSuccess(cards) {
  return { type: types.SEARCH_CARDS_SUCCESS, cards };
}
export function searchAutocompleteCardsSuccess(cards) {
  return { type: types.SEARCH_AUTOCOMPLETE_CARDS_SUCCESS, cards };
}
export function noResult() {
  return { type: types.NO_RESULT };
}
export function onResult() {
  return { type: types.ON_RESULT };
}

export function updateCardSuccess(card) {
  return { type: types.UPDATE_CARD_SUCCESS, card };
}

export function createCardSuccess(card) {
  return { type: types.CREATE_CARD_SUCCESS, card };
}

export function deleteCardOptimistic(card) {
  return { type: types.DELETE_CARD_OPTIMISTIC, card };
}
export function autocompleteCards(name = "") {
  return function (dispatch) {
    dispatch(beginApiCall());
    return cardApi
      .searchCards(name, false)
      .then((resp) => {
        const cards = resp.data.filter(
          (data) => data.lang === "fr" || data.lang === "en"
        );
        dispatch(onResult());
        dispatch(searchAutocompleteCardsSuccess(cards));
      })
      .catch((error) => {
        dispatch(noResult());
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function searchCards(name = "") {
  return function (dispatch) {
    dispatch(beginApiCall());
    return cardApi
      .searchCards(name, false)
      .then((resp) => {
        const cards = resp.data.filter(
          (data) => data.lang === "fr" || data.lang === "en"
        );
        dispatch(onResult());
        dispatch(searchCardsSuccess(cards));
      })
      .catch((error) => {
        dispatch(noResult());
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function getCard(id = "") {
  return function (dispatch) {
    dispatch(beginApiCall());
    return cardApi
      .getCard(id)
      .then((card) => {
        dispatch(onResult());
        dispatch(getCardSuccess(card));
      })
      .catch((error) => {
        dispatch(noResult());
        dispatch(apiCallError(error));
        throw error;
      });
  };
}
export function updateCardsResult() {
  return function (dispatch) {
    dispatch(fillCardsResultList());
  };
}

export function updateCardResult(card) {
  return function (dispatch) {
    dispatch(fillCardResult(card));
  };
}

export function resetAutocomplete() {
  return function (dispatch) {
    dispatch(resetCardsAutocompleteList());
  };
}
export function resetCard() {
  return function (dispatch) {
    dispatch(resetCardResult());
  };
}
export function resetCards() {
  return function (dispatch) {
    dispatch(resetCardsList());
  };
}

export function saveCard(card) {
  // eslint-disable-next-line no-unused-vars
  return function (dispatch, getState) {
    dispatch(beginApiCall());
    return cardApi
      .saveCard(card)
      .then((savedCard) => {
        card.id
          ? dispatch(updateCardSuccess(savedCard))
          : dispatch(createCardSuccess(savedCard));
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function deleteCard(card) {
  return function (dispatch) {
    // Doing optimistic delete, so not dispatching begin/end api call
    // actions, or apiCallError action since we're not showing the loading status for this.
    dispatch(deleteCardOptimistic(card));
    return cardApi.deleteCard(card.id);
  };
}
